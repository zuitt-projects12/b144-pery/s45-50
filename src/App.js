// Import the Fragment component from React
// import { Fragment } from 'react';
import { useState, useEffect } from "react";
import { Container } from "react-bootstrap";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import AppNavbar from "./components/AppNavbar";
// import Banner from "./components/Banner";
// import Highlights from "./components/Highlights";
import Courses from "./pages/Courses";
import CourseView from "./components/CourseView";
import Home from "./pages/Home";
import Register from "./pages/Register";
import Login from "./pages/Login";
import Logout from "./pages/Logout";
import Error from "./pages/Error";
import './App.css';
import { UserProvider } from "./UserContext";

function App() {
  // State hood for the user state that's define fro a global scope.
  // Initialize an object with properties from the localStorage
  const [user, setUser] = useState({
    // email: localStorage.getItem("email")
    id: null,
    isAdmin: null
  });

  // Function for clearing localStorage on Logout
  const unsetUser = () => {
    localStorage.clear();
  };

  // Used to check if the user information is properly stored upon login and the localStorage information is cleared upon logout
  useEffect(() => {
    console.log(user);
    console.log(localStorage);
  }, [user]);

  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <AppNavbar />
        <Container>
          <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/courses" component={Courses} />
            <Route exact path="/courses/:courseId" component={CourseView} />
            <Route exact path="/register" component={Register} />
            <Route exact path="/login" component={Login} />
            <Route exact path="/logout" component={Logout} />
            <Route component={Error} />
          </Switch>
        </Container>
      </Router>
    </UserProvider>
  );
}

export default App;
