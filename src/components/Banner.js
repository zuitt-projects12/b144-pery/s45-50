// Import the Button, Row, and Col components in react-bootstrap
// import Button from "react-bootstrap/Button";
// // Bootstrap grid system components
// import Row from "react-bootstrap/Row";
// import Col from "react-bootstrap/Col";

// Single importation
import { Button, Row, Col } from "react-bootstrap";
import { Link } from "react-router-dom";

const Banner = ({data}) => {
	const {title, content, destination, label} = data;
	return (
		<Row>
			<Col className="p-5">
				<h1>{title}</h1>
				<p>{content}</p>
				<Button as={Link} to={destination}>{label}</Button>
			</Col>
		</Row>
	);
}

export default Banner;