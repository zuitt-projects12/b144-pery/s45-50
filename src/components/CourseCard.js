// import state hook from react
// import { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { Card, Button } from "react-bootstrap";
import { Link } from "react-router-dom";

const CourseCard = ({ courseProp }) => {
    // Checks to see if the data was successfully passed
    // console.log(courseProp);
    // console.log(typeof courseProp);
    const {_id, name, description, price} = courseProp;
    
    // Use state hook in this component to be able to store its state. 
    // States are used to keep track of information related to individual components
    /* 
        Syntax:
        const [getter, setter] = useState(initialGetterValue);
    */
    // const [count, setCount] = useState(0);
    // const [seats, setSeats] = useState(30);
    // const [isOpen, setIsOpen] = useState(false);

    // console.log(useState(0));

    // Function to keep track of the enrollees for a course
    // function enroll() {
    //     setCount(count + 1);
    //     setSeats(seats - 1);
    // } 

    // Define a useEffect hook to have the CourseCard component perform a certain task after every DOM update
    // useEffect(() => {
    //     if(seats === 0){
    //         setIsOpen(true);
    //         setVariant("secondary");
    //     }
    // }, [seats]);

    return (
        <Card className="mb-2">
           <Card.Body>
               <Card.Title>{name}</Card.Title>
               <Card.Subtitle>Description:</Card.Subtitle>
               <Card.Text>{description}</Card.Text>
               <Card.Subtitle>Price:</Card.Subtitle>
               <Card.Text>&#8369; {price}</Card.Text>
               <Link className="btn btn-success" to={`/courses/${_id}`}>Details</Link>
           </Card.Body>
       </Card> 
    );
}

export default CourseCard;
