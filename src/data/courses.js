// Mock database.
const courses = [
    {
        id: "wdc001",
        name: "PHP - Laravel",
        description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Ab, quaerat ea animi quisquam earum saepe asperiores consectetur quod a aut, voluptas quidem fuga? Cumque necessitatibus provident corrupti deserunt accusantium perspiciatis natus voluptate. Dolor cum dolorem, tenetur unde deleniti similique repudiandae, possimus ipsam sint tempore, fugiat beatae impedit error quaerat ipsa! Nisi, quas atque. Pariatur qui obcaecati deserunt laudantium iure excepturi voluptate dolore suscipit cupiditate recusandae amet perspiciatis ut ea architecto blanditiis ab, quisquam quo maxime explicabo laboriosam. Quidem, tenetur similique, deserunt earum ipsum vero, consequatur quo libero maiores soluta pariatur perspiciatis distinctio! Quidem sunt aperiam atque ipsam nobis, neque libero!?",
        price: 45000,
        onOffer: true
    },
    {
        id: "wdc002",
        name: "Python - Flask",
        description: "Kasi , doonek ang na ang pranella ano bokot bakit neuro emena gushung na at nang majubis bakit krung-krung chipipay na nang at ang at bakit planggana pamin ano nang wiz jutay daki nakakalurky jowa ano dites at nang bonggakea paminta at na guash urky at mabaho doonek tanders katol na ang bonggakea thunder sa jupang-pang bella shokot shogal shonget ng na ang at nang nang at bakit tetetet kasi at bakit ma-kyonget antibiotic sa bakit ang chuckie at bakit katagalugan jowabella conalei pamenthol ganders daki katol ang at nang shokot wiz makyonget jupang-pang ng ano kasi shontis jowabella emena gushung ang urky at bakit matod jongoloids 48 years paminta , ugmas bigalou at ang antibiotic bella na ang daki shontis chipipay borta emena gushung na nang mashumers Mike sa sangkatuts jowabelles krung-krung bongga nakakalurky kasi pranella sa tamalis krung-krung at intonses ang shokot keme fayatollah kumenis nang krung-krung wasok shongaers ugmas tanders chipipay lorem ipsum keme keme shonga at bakit jowabella at bakit mabaho mashumers sa at bakit mahogany daki balaj ang mashumers bella chopopo at ang nang shonget jongoloids na jowabella kabog otoko planggana ng nakakalurky kasi jongoloids keme nang kirara cheapangga warla juts nang nang shonget na shonget jowabelles quality control shogal ng bonggakea 48 years at ang chopopo at bakit at bakit sa sa ano Cholo borta buya pinkalou nang ng chapter oblation shontis kasi fayatollah kumenis intonses fayatollah kumenis.",
        price: 50000,
        onOffer: true
    },
    {
        id: "wdc003",
        name: "Java - Springboot",
        description: "Chuckie thunder at nang na ang jowa bigalou at ang balaj quality control jongoloids katagalugan dites majubis emena gushung ng ano guash mahogany bella jowabelles paminta ang at tamalis krung-krung sangkatuts tamalis warla emena gushung at bakit na ang tanders bakit na ang ma-kyonget at nang ma-kyonget jongoloids at at nang pamenthol daki pamentos wiz bokot nang tamalis kirara bakit jupang-pang buya Gino makyonget kasi sa emena gushung chuckie sangkatuts biway tetetet quality control chopopo warla ng Mike ang juts shala kemerloo kabog effem sa ng chapter 48 years klapeypey-klapeypey katol sa at bakit planggana ano dites ano effem na keme keme kasi shokot wasok ganda lang ng kasi na wiz at bakit na ang bakit kasi keme wasok mahogany ang at bakit at majubis sangkatuts Gino oblation bokot at nang shala jowa mahogany na ang sa chaka at ang shala at at nang ang daki makyonget shongaers ang bigalou pamenthol sa lulu sa shala doonek buya ang pamentos ng keme bonggakea at nang emena gushung wiz ang ano ang kabog shala at ang keme keme mabaho valaj sudems 48 years nang lulu at ang kasi mashumers ang urky chuckie bigalou wiz 48 years guash buya jutay oblation na at nang bigalou sa wasok at 48 years at ang guash bella wrangler borta at ang borta chipipay krung-krung pamentos kasi shokot sa tamalis at ang kasi at sa paminta antibiotic at ang pamin at at jutay sudems.",
        price: 45000,
        onOffer: true
    },
    {
        id: "wdc004",
        name: "HTML Introduction",
        description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Quam voluptatem maiores sequi nesciunt quod totam numquam delectus, enim culpa magnam. Officiis hic neque quia voluptatibus accusantium, molestias nihil eveniet eius suscipit ratione iusto nam iure et! Cum eligendi, tempora corporis accusantium tenetur magnam. Deserunt laboriosam magni rem, tempore qui numquam.",
        price: 30000,
        onOffer: true
    }
];

export default courses;
