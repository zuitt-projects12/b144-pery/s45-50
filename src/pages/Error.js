import Banner from "../components/Banner";

const Error = () => {
    const data = {
        title: "404 - Page Not Found",
        content: "The page you are looking for is not found",
        destination: "/",
        label: "Go back home"
    }
    return (  
        <Banner data={data}/>
    )
}

export default Error;
