import { Fragment } from "react";
import Banner from "../components/Banner";
import Highlights from "../components/Highlights";
// import CourseCard from "../components/CourseCard";

const Home = () => {
    const data = {
        title: "Zuitt Coding Bootcamp",
        content: "Opportunities for everyone, everywhere",
        destination: "/courses",
        label: "Enroll Now!"
    }
    return (
        <Fragment>
            <Banner data={data} />
            <Highlights />
        </Fragment>
    );
}

export default Home;
