import { useState, useEffect, useContext } from "react";
import { Form, Button, Container } from "react-bootstrap";
import { Redirect } from "react-router-dom"; 
import Swal from "sweetalert2";
import UserContext  from "../UserContext";

const Login = (props) => {
    // Allows to consume the UserContext object and it's properties to use for user validation
    const {user, setUser} = useContext(UserContext);

    // State hooks to stre the values of the input fields
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [isActive, setIsActive] = useState(false);
    const [variant, setVariant] = useState("danger");

    function loginUser(e) {
        // Prevents page redirection via form submission
        e.preventDefault();

        // Process a fetch request to the corresponding backend API.
        /* 
            Syntax:
            fetch("url", {options})
                .then(res => res.json())
                .then(data => {data})
        */
        fetch("http://localhost:4000/api/users/login", {
           method: "POST",
           headers: {
               "Content-Type": "application/json"
           },
           body: JSON.stringify({
               email: email,
               password: password
           })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data.accessToken);
            if(typeof data.accessToken !== "undefined"){
                localStorage.setItem("token", data.accessToken);
                retrieveUserDetails(data.accessToken);
                console.log(`ID: ${data.id}`);
                console.log(`Admin: ${data.isAdmin}`);

                Swal.fire({
                    title: "Login Successful",
                    icon: "success",
                    text: "Welcome to Zuitt!"
                });
            }
            else{
                Swal.fire({
                    title: "Authentication failed",
                    icon: "error",
                    text: "Check your login details and try again."
                });
            }
        });

        // Set the email of the authenticated user in the local storage.
        /* 
            Syntax:
            localStorage.setItem('propertyName', value)
        */
        // localStorage.setItem("email", email);

        // Set the global user state to have properties obtained from local storage.
        // setUser({
        //     email: localStorage.getItem("email")
        // });

        // Clear input fields
        setEmail("");
        setPassword("");

        // alert("You are now logged in!");
    }

    const retrieveUserDetails = (token) => {
        fetch("http://localhost:4000/api/users/details", {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })
        });
    };

    useEffect(() => {
        if(email === "" || password === ""){
            setIsActive(false);
            setVariant("danger");
        }
        else{
            setIsActive(true);
            setVariant("success");
        }
    }, [email, password]);

    return (
        (user.id !== null) ? 
            <Redirect to="/courses"/>
            :
        <Container>
            <h1 className="text-center py-3">Login</h1>
            <Form onSubmit={(e) => loginUser(e)}>
                <Form.Group className="mb-3" controlId="userEmail">
                    <Form.Label>Email</Form.Label>
                    <Form.Control 
                        type="email"
                        value={email}
                        onChange={e => setEmail(e.target.value)}
                        placeholder="johndoe@mail.com"
                        required
                    />
                </Form.Group>

                <Form.Group className="mb-3" controlId="password">
                    <Form.Label>Password</Form.Label>
                        <Form.Control 
                            type="password"
                            value={password}
                            onChange={e => setPassword(e.target.value)}
                            placeholder="hard to guess string"
                            required
                        />
                </Form.Group>

                { isActive ? 
                    <Button 
                        variant={variant}
                        type="submit"
                        id="submitBtn"
                    >
                        Submit
                    </Button>
                :
                    <Button 
                        variant={variant}
                        type="submit"
                        id="submitBtn"
                        disabled
                    >
                        Submit
                    </Button>
                }
            </Form>
        </Container>
    )
}

export default Login;
