import { useContext, useEffect } from "react";
import { Redirect } from "react-router-dom";
import UserContext from "../UserContext";

const Logout = () => {
    // localStorage.clear();

    const { unsetUser, setUser } = useContext(UserContext);

    // Clear the localStorage of the user's information
    unsetUser();

    useEffect(() => {
        // Set user state back to its original value
        setUser({id: null});
    });

    // Redirect back to login
    return (
        <Redirect to="/login" />
    );
}

export default Logout;
