import { useState, useEffect, useContext } from "react";
import { Redirect } from "react-router-dom";
import { Form, Button, Container } from "react-bootstrap";
import { useHistory } from "react-router-dom";
import UserContext from "../UserContext"; 
import Swal from "sweetalert2";

const Register = () => {
    const {user} = useContext(UserContext);
    const history = useHistory();
    // State hooks to stroe the values of the input fields.
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [age, setAge] = useState(0);
    const [email, setEmail] = useState("");
    const [gender, setGender] = useState("");
    const [mobileNo, setMobileNo] = useState("");
    const [password1, setPassword1] = useState("");
    const [password2, setPassword2] = useState(""); 
    // State to determine submit button is enabled or not
    const [isActive, setIsActive] = useState(false);
    const [variant, setVariant] = useState("danger");

    // Check if values are successfully binded.
    // console.log(`Email: ${email}`);
    // console.log(`Password 1: ${password1}`);
    // console.log(`Password 2: ${password2}`);

    // Function to simulate user registration
    async function registerUser(e) {
        // Prevents page redirection via form submission
        e.preventDefault();
    

        // call /api/users/checkemail
        const checkEmailExists = await fetch("http://localhost:4000/api/users/checkEmail", {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                email: email
            })
        })
        .then(res => res.json())
        .then(data => data);
        if (!checkEmailExists){
            // TODO: call /api/users/register if email does not exist.
            fetch("http://localhost:4000/api/users/register", {
                method: "POST",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify({
                    firstName: firstName,
                    lastName: lastName,
                    age: age,
                    email: email,
                    gender: gender,
                    mobileNo: mobileNo,
                    password: password2
                })
            })
            .then(res => res.json())
            .then(data => {
                console.log(data);
                Swal.fire({
                    title: "Registration successful!",
                    icon: "success",
                    text: "You can now login and enroll courses!"
                });
                history.push("/login")
            });

        }
        else{
            Swal.fire({
                title: "Email Is Already Registered!",
                icon: "error",
                text: "Please select another email."
            });
        }
         

        // Clear input fields
        // setEmail("");
        // setPassword1("");
        // setPassword2("");

        // alert("Thank you for registering!");
    }

    useEffect(() => {
        // Validation to enable the submit button when all fields are populated and both passwords match
        if((firstName !== "" && lastName !== "" && email !== "" && gender !== "" && mobileNo !== "" && password1 !== "" && password2 !== "") && (password1 === password2)){
            setIsActive(true);
            setVariant("success");
        }
        else{
            setIsActive(false);
            setVariant("danger");
        }
    }, [email, password1, password2]);

    return (
        (user.id !== null) ?
            <Redirect to="/courses"/>
        :
        <Container>
            <h1 className="text-center py-3">Register</h1>
            <Form onSubmit={(e) => registerUser(e)}>
                <Form.Group className="mb-3" controlId="userFirstName">
                    <Form.Label>First Name</Form.Label>
                    <Form.Control 
                        type="text" 
                        placeholder="John" 
                        value = {firstName}
                        onChange = {e => setFirstName(e.target.value)}
                        required 
                    />
                </Form.Group>
                <Form.Group className="mb-3" controlId="userLastName">
                    <Form.Label>Last Name</Form.Label>
                    <Form.Control 
                        type="text" 
                        placeholder="Doe" 
                        value = {lastName}
                        onChange = {e => setLastName(e.target.value)}
                        required 
                    />
                </Form.Group>
                <Form.Group className="mb-3" controlId="userAge">
                    <Form.Label>Age</Form.Label>
                    <Form.Control 
                        type="number" 
                        placeholder="18" 
                        value = {age}
                        onChange = {e => setAge(e.target.value)}
                        required 
                    />
                </Form.Group>
                <Form.Group className="mb-3" controlId="userEmail">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control 
                        type="email" 
                        placeholder="johndoe@mail.com" 
                        value = {email}
                        onChange = {e => setEmail(e.target.value)}
                        required 
                    />
                    <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                    </Form.Text>
                </Form.Group>
                <Form.Group className="mb-3" controlId="userMobileNo">
                    <Form.Label>Mobile number</Form.Label>
                    <Form.Control 
                        type="text" 
                        placeholder="09171234567" 
                        value = {mobileNo}
                        onChange = {e => setMobileNo(e.target.value)}
                        required 
                    />
                    <Form.Text className="text-muted">
                    We'll never share your phone number with anyone else.
                    </Form.Text>
                </Form.Group>

                <Form.Group className="mb-3" controlId="userGender">
                    <Form.Label>Gender</Form.Label>
                    <Form.Select 
                        aria-label="Default select example" 
                        value={gender}
                        onChange = {e => setGender(e.target.value)}
                        required
                    >
                        <option value="">Open this select menu</option>
                        <option value="male">Male</option>
                        <option value="female">Female</option>
                        <option value="other">Other</option>
                    </Form.Select>
                </Form.Group>

                <Form.Group className="mb-3" controlId="password1">
                    <Form.Label>Password</Form.Label>
                    <Form.Control 
                        type="password" 
                        placeholder="Password"
                        value = {password1}
                        onChange = {e => setPassword1(e.target.value)}
                        required 
                    />
                </Form.Group>

                <Form.Group className="mb-3" controlId="password2">
                    <Form.Label>Verify Password</Form.Label>
                    <Form.Control 
                        type="password" 
                        placeholder="Verify Password" 
                        value = {password2}
                        onChange = {e => setPassword2(e.target.value)}
                        required 
                    />
                </Form.Group>
                {/* Conditionally render submit button based on isActive state. */}
                { isActive ?     
                    <Button 
                        variant={variant} 
                        type="submit" 
                        id="submitBtn"
                    >
                        Submit
                    </Button>
                    :
                    <Button 
                        variant={variant} 
                        type="submit" 
                        id="submitBtn"
                        disabled
                    >
                        Submit
                    </Button>
                }
            </Form>
        </Container>
    );
}

export default Register;
